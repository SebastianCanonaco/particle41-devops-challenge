import { Injectable } from '@nestjs/common';
import { ResponseDto } from './dtos/response.dto';

@Injectable()
export class AppService {
  get(ip): ResponseDto {
    let response: ResponseDto = new ResponseDto();
    response.ip = ip
    response.timestamp = new Date()
    return response;
  }
}
