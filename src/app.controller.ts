import { Controller, Get, Req } from '@nestjs/common';
import { AppService } from './app.service';
import { ResponseDto } from './dtos/response.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  get(@Req() req): ResponseDto {
    return this.appService.get(req.connection.remoteAddress);
  }
}
