<h2>This project is part of the Particle41 hiring process</h2>

<p>
If you wanna read full description of the challenge go to
<a href="https://bitbucket.org/particle41/devops-challenge-entry/src/main/" target="_blank">this</a> link
</p>

<h2>Microservice</h3>
<p>For the microservice I have chosen NestJS, which is a Nodejs framework.
You can find the full documentation and the setup guide <a href="https://docs.nestjs.com/first-steps" target="_blank">here.</a>
</p>

<p>NestJS uses typescript as is coding language. We will use <a href="https://yarnpkg.com/" target="_blank">yarn</a> for package and dependencies manager</p>

<p>The application would be running inside a Docker container within a Kubernetes cluster. Docker is a container runtime. Kubernetes is a container orchestration engine.
You can also find the official documentation and setup guides below:
</p>
<ul>
    <li><a href="https://www.docker.com/get-started" target="_blank">Docker</a></li>
    <li><a href="https://kubernetes.io/docs/home/" target="_blank">Kubernetes</a></li>
</ul>

<p>If you want to run the complete application including Docker and Kubernetes, you need have installed:
<ul>
<li>NodeJS.</li>
<li>npm and yarn.</li>
<li>Nest CLI.</li>
<li>Docker Engine (Linux) or Docker Desktop (Windows and Mac user, which also includes Kubernetes engine).</li>
<li>Kubernetes cluster (Linux). <a href="https://minikube.sigs.k8s.io/docs/start/" target="_blank">Minikube</a>, for example.</li>
</ul>
</p>

The steps I have followed to push the image of the microservice are the next ones:
<ul>
    <li>Create a Dockerfile to build the image of the application.</li>
    <li>Create a <a href="https://hub.docker.com/" target="_blank">Docker Hub</a> account and create a public repository in it.</li>
    <li>Build the application with the following command to generate a tagged (based on previously repository) docker image:</li>
    <ul>
    <li>
    docker build -t sebastiancanonaco/particle41-service:v1 &ltpath-to-Dockerfile&gt
    </li>
</ul>

<li>Login into Docker Hub with "docker login" command and input username and password.</li>
<li>Push the image to Docker Hub with "docker push sebastiancanonaco/particle41-service:v1" command.</li>
</ul>

Now we have our Docker image on Docker Hub. The next steps would be to deploy that image to our local Kubernetes cluster.
We will use the kubectl command tool, be sure to check you already have it, with "kubectl version". If you are using Minikube, maybe you would need to use "minikube kubectl" command, you could also attach an alias to simplify the syntax (alias kubectl="minikube kubectl --").

<li>First, start your Kubernetes cluster, check the links provided above for both Docker Desktop and Minikube.</li>

<li>There is a manifest file, for Kubernetes inside the 'k8s' folder called microservice.yml. Once started, run the following command: "kubectl apply -f &ltpath-to-manifest.yml&gt"</li>

<li>If everything went well, you should be able to run this command "kubectl get pod" and see and output like this</li>

```console
$ kubectl get pod
NAME                       READY   STATUS        RESTARTS   AGE
rest-api-cf649c8b5-hzftm   1/1     Running   0          85s
```

If STATUS is in Creating state, wait a bit of time and try again. If there is an error, you can check the logs and pod state with "kubectl logs" or "kubectl describe" commands and trace the error to solve it.

Now our application is running within the cluster and inside the container, but we can only access it through the Kubernetes Service resource that we created on the manifest.
So a quick solution would be to execute this command:

```console
$ kubectl port-forward svc/rest-api 80:80
```

Now we are forwanding all request done to localhost on port 80 to the service which is also being hosted on port 80 inside the cluster.

Now we can access on the browser or do a curl command to "localhost:80/" and se the desired response.

Thanks for joining, improves are always welcome and you can contact me by email: sebastiancanonaco@gmail.com